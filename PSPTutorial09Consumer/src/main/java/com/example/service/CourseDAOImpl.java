package com.example.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.dao.CourseDAO;
import com.example.model.CourseModel;

@Service
public class CourseDAOImpl implements CourseDAO
{
	@Bean(name="CourseBean")
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	@Autowired
	private RestTemplate restTemplate = this.restTemplate();
	
	@Override
	public CourseModel selectCourse (String id)
	{
		CourseModel course =
			restTemplate.getForObject(
			"http://localhost:9090/rest/course/view/"+id,
			CourseModel.class);
		return course;
	}
	
	@Override
	public List<CourseModel> selectAllCourses ()
	{
		CourseModel[] courses =
				restTemplate.getForObject(
				"http://localhost:9090/rest/course/viewall",
				CourseModel[].class);
		return Arrays.asList(courses);
	}
}

